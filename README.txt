-Liens importants-

https://gitlab.com/ai110_projet1_tc_sh_jbh/ai110_projet1.git
https://trello.com/projetannuaireagile


-Structure du dossier 'projet1'-

>1_Mise_en_place
>fichiers
>src
.gitignore
BurnDownChart projet1.xlsx
README.txt


-Travail individuel-

git checkout master
git pull //copier le src avec toutes les fonctionalités fusionnées sur le bureau
//on travaille dans un nouveau package pour éviter de créer des conflits
git checkout <prenom>
// une fois terminé, on écrase le src présent dans notre branche
git add *
git commit -m "description"
git push // le src est donc à jour dans notre branche


-Merge des travaux-

// en fin de journée seulement et AVEC TOUTE LA TEAM
git checkout <prenom>
// on enlève du src les packages susceptibles de créer des conflits
git add *
git commit -m "prep merge"
git push
// on se place SYSTEMATIQUEMENT dans la master avant de faire un merge
git checkout master
git merge <prenom>
git push
// on copie le nouveau src dans le bureau pour être parfaitement à jour
git checkout <prenom> // et on se replace dans sa branche





//Tips:
//Première ouverture seulement
//git clone pour travailler en local (dans le dossier de votre choix)
//git branch <prenom> (par exemple)
//git checkout <prenom> (n'hesitez pas à faire un git status pour bien vérifier dans quelle branche vous êtes)


