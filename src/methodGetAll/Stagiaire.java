package methodGetAll;

import java.nio.charset.StandardCharsets;

public class Stagiaire {
	
	private String lastName;
	private String firstName;
	private String department;
	private String promoName;
	private String year;
	
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getPromoName() {
		return promoName;
	}
	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	@Override
	public String toString() {
		
		String spaceLN ="";
		String spaceFN ="";
		String spaceDPT ="";
		String spacePN ="";
		String spaceYR ="";
		
		for(int i =0;i<(30-lastName.length());i++) spaceLN +=" ";
		for(int i =0;i<(30-firstName.length());i++) spaceFN +=" ";
		for(int i =0;i<(30-department.length());i++) spaceDPT +=" ";
		for(int i =0;i<(30-promoName.length());i++) spacePN +=" ";
		for(int i =0;i<(30-year.length());i++) spaceYR +=" ";
		
		return lastName+spaceLN+firstName+spaceFN+department+spaceDPT+promoName+spacePN+year+spaceYR;
	}
	public Stagiaire() {
		super();
	}
	public Stagiaire(String lastName, String firstName, String department, String promoName, String year) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
		this.department = department;
		this.promoName = promoName;
		this.year = year;
	}
	public String toString2() {
        return new String ((lastName + " `` " + firstName + " `` " + department + " `` " + promoName + " `` " + year).getBytes(),StandardCharsets.ISO_8859_1);
    }
}
