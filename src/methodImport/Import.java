package methodImport;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.TextField;
import methodGetAll.Stagiaire;
import userInterface.MainApp;

public class Import {

	static RandomAccessFile raf;

	static List<Stagiaire> stagiaires = new ArrayList<Stagiaire>();
	public static int posDepart;
	static int posParent;
	


	//Champs

	//
	String noeudGauche;
	String noeudDroit;

	// TODO initialiser les champs vides
	static String emptyField = "                              ";
	static String emptyNode ="                                                                                                                                                                                                                  ";
	static byte[] bt = new byte[150];
	int dbRoot = 0;
	int	lengthField = 30;	


	String sourcePathFile = MainApp.relativepath + "/Workspace_Projet1/Source/stagiaires.txt";


	public Import() {
		//this.raf = raf;
		try {
			raf = new RandomAccessFile(MainApp.relativepath + "/Workspace_Projet1/Source/Raf.bin", "rw");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public void createStagiaire(List<TextField> tfs) throws IOException {
		long rafEnd = raf.length();
		for(int j=0;j<5;j++) {
			raf.seek(raf.length());
			if (j==0) raf.writeBytes(tfs.get(j).getText().trim().toUpperCase());
			else raf.writeBytes(tfs.get(j).getText().trim());
			for (int i = 0; i < (lengthField - tfs.get(j).getText().trim().length());i++) raf.writeBytes(" ");

		}
		for (int i = 0; i < lengthField * 2 ; i++) raf.writeBytes(" ");
		positionNode(rafEnd, dbRoot);
	}


	public void importFileStagiaire() throws IOException {

		try {

			String txt = "";
			BufferedReader br = new BufferedReader(new FileReader(sourcePathFile));
			while(br.ready()){ // tant que pas a la fin du fichier
				long rafEnd = raf.length();
				for(int j=0;j<5;j++) {
					if (j==0) txt = new String (br.readLine().trim().toUpperCase().getBytes(),StandardCharsets.UTF_8);
					else txt = new String (br.readLine().trim().getBytes(),StandardCharsets.UTF_8);

					raf.seek(raf.length());

					raf.writeBytes(txt);
					for (int i = 0; i < (lengthField - txt.length());i++) raf.writeBytes(" ");
				} 
				for (int i = 0; i < lengthField * 2 ; i++) raf.writeBytes(" ");
				positionNode(rafEnd, dbRoot);
				br.readLine();
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}

	//une methode qui positionne un Node dans la base de donn�e 
	public static void positionNode(long rafEnd, int pos) throws IOException {
		// on verifie la racine
		if (rafEnd != pos ) {

			if (getStringStagiaire(rafEnd).compareTo(getStringStagiaire(pos)) < 0) {
				if (getBrancheGauche(pos) == -1) {
					raf.seek(pos +150);
					raf.writeBytes(Integer.toString((int) rafEnd));
				} else positionNode(rafEnd,getBrancheGauche(pos));

			}else {

				if (getBrancheDroit(pos)==-1) {
					raf.seek(pos + 180);
					raf.writeBytes(Integer.toString((int) rafEnd));
				} else positionNode(rafEnd,getBrancheDroit(pos));

			}
		}

	}

	//une methode qui renvoie sous forme de string un stagiaire en entier
	public static String getStringStagiaire(long pos) {
		try {
			raf.seek(pos);
			byte[] bt = new byte[150];
			raf.read(bt,0,150);
			String ref = new String(bt);
			return ref;
		} catch (IOException e) {
			return null;
		}
	}

	//une methode qui renvoie la position de la branche gauche sous forme de int
	public static int getBrancheGauche(int pos) {

		try {
			long initpos = raf.getFilePointer();
			int posGauche = pos + 150;
			raf.seek(posGauche);
			byte[] bt = new byte[30];
			raf.read(bt,0,30);
			String noeudGauche = new String(bt,StandardCharsets.ISO_8859_1); // On pointe sur l'emplacement gauche 
			if (noeudGauche.compareTo(emptyField) == 0) {
				raf.seek(initpos);
				return -1;
				
			}
			raf.seek(initpos);
			return Integer.parseInt(noeudGauche.trim());
		
		} catch (IOException e) {
			return -1;
		}


	}

	//une methode qui renvoie la position de la branche droite sous forme de int
	public static int getBrancheDroit(int pos) {

				

		try {
			long initpos = raf.getFilePointer();
			int posDroit = pos + 180;
			raf.seek(posDroit);
			byte[] bt = new byte[30];
			raf.read(bt,0,30);
			String noeudDroit = new String(bt,StandardCharsets.ISO_8859_1); // On pointe sur l'emplacement gauche 
			if (noeudDroit.compareTo(emptyField) == 0) {
				raf.seek(initpos);
				return -1;
			}
			raf.seek(initpos);
			return Integer.parseInt(noeudDroit.trim());
		
		} catch (IOException e) {
			return -1;
		}
		
	}	


	//une methode de recherche qui renvoie une liste de Stagiaires correspondant au crit�re mis dans "recherche"
	public static List<Stagiaire>searchNode(String recherche, int posDepart,int deb,int fin) throws IOException {
		try {
			raf = new RandomAccessFile(MainApp.relativepath + "/Workspace_Projet1/Source/Raf.bin", "rw");
			searchNodeLinear(recherche, posDepart,deb,fin);
			return stagiaires;
		} catch (FileNotFoundException e) {
			return null;
		}

	}

	//une methode de recherche qui renvoie une liste de Stagiaires correspondant au crit�re mis dans "recherche"
	public static List<Stagiaire>searchNodeLastName(String recherche, int posDepart) throws IOException {
		try {
			raf = new RandomAccessFile(MainApp.relativepath + "/Workspace_Projet1/Source/Raf.bin", "rw");
			searchNodeTree(recherche, posDepart);
			return stagiaires;
		} catch (FileNotFoundException e) {
			return null;
		}

	}


	//une methode de recherche qui renvoie une liste de Stagiaires correspondant au crit�re mis dans "recherche"
	public static void searchNodeTree(String recherche, int posDepart) throws IOException {

		if(posDepart != -1) {

			int test = getStringStagiaire(posDepart).substring(0,30).trim().compareTo(recherche);
			List<String> s = new ArrayList<String>();
			if (test == 0) {
				for (int i=0;i<150;i+=30) s.add(getStringStagiaire(posDepart).substring(i,i+30));
				Stagiaire Stemp = new Stagiaire(s.get(0).toString(),s.get(1).toString(),s.get(2).toString(),s.get(3).toString(),s.get(4).toString());	
				stagiaires.add(Stemp);
				searchNodeLastName(recherche, getBrancheDroit(posDepart));
			} else if (test > 0) searchNodeLastName(recherche, getBrancheGauche(posDepart));
			else searchNodeLastName(recherche, getBrancheDroit(posDepart));
		}
	}

	//une methode de recherche qui renvoie une liste de Stagiaires correspondant au crit�re mis dans "recherche"
	public static void searchNodeLinear(String recherche, int posDepart,int deb,int fin) throws IOException {

		if(posDepart < raf.length()-200) {		
			//System.out.println(getStringStagiaire(posDepart).substring(deb,fin).trim());

			int test = getStringStagiaire(posDepart).substring(deb,fin).trim().compareTo(recherche);
			List<String> s = new ArrayList<String>();
			if (test == 0) {
				for (int i=0;i<150;i+=30) s.add(getStringStagiaire(posDepart).substring(i,i+30));
				Stagiaire Stemp = new Stagiaire(s.get(0).toString(),s.get(1).toString(),s.get(2).toString(),s.get(3).toString(),s.get(4).toString());	
				stagiaires.add(Stemp);
				searchNodeLinear(recherche, posDepart+210,deb,fin);
			}else searchNodeLinear(recherche, posDepart+210,deb,fin);
		}
	}


	//Une methode qui prend en entr�e une string de stagiaire, 
	//et qui la modifie pour obtenir la position en byte de la string recherch�e et la position en byte du noeud parent de la string recherch�e
	public static void searchNodePositionUtil(String recherche) throws IOException {
		if(posDepart !=-1) {
			int test = new String (getStringStagiaire(posDepart).getBytes(),StandardCharsets.UTF_8).compareTo(recherche);
			//System.out.println("je compare \r\n\t"+recherche+"\r\n avec :\r\n\t"+new String (getStringStagiaire(posDepart).getBytes(),StandardCharsets.UTF_8)+"\r\n et le r�sultat est \t:" + test);
			if (test==0) {} 
			else if (test > 0) {
				posParent=posDepart;
				posDepart = getBrancheGauche(posDepart);
				searchNodePositionUtil(recherche);
			}
			else {
				posParent=posDepart;
				posDepart=getBrancheDroit(posDepart);
				searchNodePositionUtil(recherche);
			}
		}
	}

	public static void deleteNode(String recherche) throws IOException {
		try {

			raf = new RandomAccessFile(MainApp.relativepath + "/Workspace_Projet1/Source/Raf.bin", "rw");
			int reposition = 0;

			searchNodePositionUtil(recherche);
			//System.out.println("la pos de d�part est "+posDepart+"le stagiaire � supprimer est "+recherche);
			if (posDepart == -1 ) {
				posDepart=0;
			}
			else {

				if (getBrancheDroit(posDepart) == -1&&getBrancheGauche(posDepart) == -1) {//si le stagiaire n'a pas de branche droite d�s le d�part de la recherche (cas ou il n'y a qu'un stagiaire)
					raf.seek(posDepart);//on se positionne sur le stagiaire
					raf.writeBytes(emptyNode);// on supprime le stagiaire en �crivant du vide
					if (posParent!=0) raf.seek(posParent+150); // on se positionne sur le noeud parent
					raf.writeBytes(emptyField); // on suprime la r�f�rence au stagiaire de droite sur l'emplacement "droite" du stagiaire parent (vu qu'on vient de le suprimer)
					posParent = 0;
					posDepart = 0;


				}else if (getBrancheDroit(posDepart) == -1) {
					if (getBrancheDroit(getBrancheGauche(posDepart)) == -1) { //si le stagiaire de gauche n'a pas de branche droite
						raf.seek(getBrancheGauche(posDepart)); //on se positionne sur le stagiaire de gauche
						raf.read(bt,0,150);//on lis le stagiaire de gauche
						String LigneADeplacer= new String(bt,StandardCharsets.ISO_8859_1); //on met le stagiaire de gauche dans une variable
						raf.seek(getBrancheGauche(posDepart));//on se repositionne sur le d�but du stagiaire de gauche
						if (getBrancheGauche(getBrancheGauche(posDepart)) != -1) reposition = getBrancheGauche(getBrancheGauche(posDepart));//on v�rifie qu'il n'ai pas d'enfant � gauche
						raf.writeBytes(emptyNode); // on supprime le stagiaire de gauche en �crivant du vide
						raf.seek(posDepart+180); // on se positionne sur le noeud parent du stagiaire de gauche, c'est a dire le stagiaire initial de recherche.
						raf.writeBytes(emptyField); // on suprime la r�f�rence au stagiaire de gauche sur l'emplacement "gauche" du stagiaire parent (vu qu'on vient de le suprimer)
						raf.seek(posDepart);//on se remet sur le stagiaire d'origine
						raf.writeBytes(LigneADeplacer);//on �crit le stagaire de gauche � la place
						if (reposition !=0)positionNode(reposition,0);
						posParent = 0;
						posDepart = 0;
					} else {
						supraDroit(getBrancheGauche(posDepart),posDepart );//sinon on va aller � gauche ind�finiement (donc recursif
					}

				}else { //sinon on va essayer d'aller a droite
					if (getBrancheGauche(getBrancheDroit(posDepart)) == -1) { //si le stagiaire de droite n'a pas de branche gauche
						raf.seek(getBrancheDroit(posDepart)); //on se positionne sur le stagiaire de droite
						raf.read(bt,0,150);//on lis le stagiaire de droite
						String LigneADeplacer= new String(bt,StandardCharsets.ISO_8859_1); //on met le stagiaire de droite dans une variable
						raf.seek(getBrancheDroit(posDepart));//on se repositionne sur le d�but du stagiaire de droite
						if (getBrancheDroit(getBrancheDroit(posDepart)) != -1) reposition = getBrancheDroit(getBrancheDroit(posDepart));//on v�rifie qu'il n'ai pas d'enfant � gauche	
						raf.writeBytes(emptyNode); // on supprime le stagiaire de droite en �crivant du vide
						raf.seek(posDepart+150); // on se positionne sur le noeud parent du stagiaire de droite, c'est a dire le stagiaire initial de recherche.
						raf.writeBytes(emptyField); // on suprime la r�f�rence au stagiaire de droite sur l'emplacement "droite" du stagiaire parent (vu qu'on vient de le suprimer)
						raf.seek(posDepart);//on se remet sur le stagiaire d'origine
						raf.writeBytes(LigneADeplacer);//on �crit le stagaire de droite � la place
						if (reposition !=0)positionNode(reposition,0);
						posParent = 0;
						posDepart = 0;
					} else {
						supraGauche(getBrancheDroit(posDepart),posDepart );//sinon on va aller � gauche ind�finiement (donc recursif
					}
				}
			}
		} catch (FileNotFoundException e) {

		}

	}
	//methode qui v�rifie si le stagiaire � une branche � gauche, si c'est le cas il avance sinon fait une sorte de "copi�", 
	//cad qu'il prend la valeur du stagiaire dans une variable 
	//et il change la r�f�rence de son stagiaire parent pour qu'il ne pointe plus sur lui

	public static void supraGauche(int PosNodeRecup,int PosNodeParentSuprim) throws IOException {
		int reposition = 0;
		if (getBrancheGauche(PosNodeRecup) == -1) { //si le stagiaire n'a pas de branche gauche
			raf.seek(PosNodeRecup); //on se positionne sur le stagiaire
			raf.read(bt,0,150);//on lis le stagiaire
			String LigneADeplacer= new String(bt,StandardCharsets.ISO_8859_1); //on met le stagiaire dans une variable
			if (getBrancheDroit(PosNodeRecup)!= -1) reposition = getBrancheDroit(PosNodeRecup);//on v�rifie qu'il n'ai pas d'enfant � droite, si oui on l'enregistre pour le repositionner � la fin
			raf.seek(PosNodeRecup);//on se repositionne sur le d�but du stagiaire
			raf.writeBytes(emptyNode); // on supprime le stagiaire en �crivant du vide
			raf.seek(PosNodeParentSuprim+150); // on se positionne sur le noeud parent du stagiaire
			raf.writeBytes(emptyField); // on suprime la r�f�rence au stagiaire sur l'emplacement "gauche" du stagiaire parent (vu qu'on vient de le suprimer)
			raf.seek(posDepart);//on se remet sur le stagiaire d'origine
			raf.writeBytes(LigneADeplacer);//on �crit le stagaire de gauche � la place
			if (reposition !=0) {
				positionNode(reposition,0);
			}
			posParent = 0;
			posDepart = 0;


		} else {//sinon on va aller � gauche ind�finiement (donc recursif)
			PosNodeParentSuprim = PosNodeRecup;//on met en noeud parent le noeud actuel
			supraGauche(getBrancheGauche(PosNodeRecup),PosNodeParentSuprim );//et on fait la m�me sur la gauche, ind�finiement jusqu'� trouver
		}


	}

	public static void supraDroit(int PosNodeRecup,int PosNodeParentSuprim) throws IOException {
		int reposition = 0;
		if (getBrancheDroit(PosNodeRecup) == -1) { //si le stagiaire n'a pas de branche droite
			raf.seek(PosNodeRecup); //on se positionne sur le stagiaire
			raf.read(bt,0,150);//on lis le stagiaire
			String LigneADeplacer= new String(bt,StandardCharsets.ISO_8859_1); //on met le stagiaire dans une variable
			if (getBrancheGauche(PosNodeRecup)!= -1) reposition = getBrancheGauche(PosNodeRecup);//on v�rifie qu'il n'ai pas d'enfant � gauche, si oui on l'enregistre pour le repositionner � la fin
			raf.seek(PosNodeRecup);//on se repositionne sur le d�but du stagiaire
			raf.writeBytes(emptyNode); // on supprime le stagiaire en �crivant du vide
			raf.seek(PosNodeParentSuprim+180); // on se positionne sur le noeud parent du stagiaire
			raf.writeBytes(emptyField); // on suprime la r�f�rence au stagiaire sur l'emplacement "droit" du stagiaire parent (vu qu'on vient de le suprimer)
			raf.seek(posDepart);//on se remet sur le stagiaire d'origine
			raf.writeBytes(LigneADeplacer);//on �crit le stagaire de droite � la place

			if (reposition !=0) {

				positionNode(reposition,0);
			}
			posParent = 0;
			posDepart = 0;


		} else {//sinon on va aller � gauche ind�finiement (donc recursif)
			PosNodeParentSuprim = PosNodeRecup;//on met en noeud parent le noeud actuel
			supraDroit(getBrancheDroit(PosNodeRecup),PosNodeParentSuprim );//et on fait la m�me sur la gauche, ind�finiement jusqu'� trouver
		}


	}



}
