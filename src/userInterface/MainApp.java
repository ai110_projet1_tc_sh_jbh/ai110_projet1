package userInterface;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import methodImport.Import;

public class MainApp extends Application {

	/*
	 * Les diff�rentes stages sont return depuis 'InitUI' et 'MainUI', 
	 * puis charg�es dans la sc�ne principale 'rootScene'.
	 * Elles sont mises en attributs de classes 'protected' pour �tre seulement
	 * accessibles depuis le package 'userInterface'.
	 */

	public static String relativepath = ".";
	public static String a = "'./Bin/a.jpg'";
	public static String b = "'./Bin/b.jpg'";
	public static String theme = a;

	protected static File passFile= new File(relativepath + "/Bin/acs");
	protected static Scene rootScene;
	protected static Stage secondaryStage = new Stage();
	protected static String frameTitle = "Utilisateur : Gestionnaire de stagiaires";

	@Override
	public void start(Stage primaryStage) throws Exception {	// review 'throw'

		/*
		 * Au premier lancement 'InitUI', on v�rifie que la bdd, la raf et
		 * mot de passe ne sont pas pr�sents. Si la raf ou le mot de passe
		 * n'existe pas, alors on vide le workspace et on ouvre la fen�tre de 
		 * d�marrage. Sinon on ouvre la fen�tre principale.
		 */

		File bdd = new File(relativepath + "/Workspace_Projet1/Source/stagiaires.txt"); 
		File rafCtrl = new File(relativepath + "/Workspace_Projet1/Source/raf.bin"); 
		File passCtrl = new File(relativepath + "/Bin/acs"); 

		if (!(bdd.exists())||!(rafCtrl.exists())||!(passCtrl.exists())) {
			if (rafCtrl.exists()) rafCtrl.delete();
			if (passCtrl.exists()) passCtrl.delete();
			InitUI.initApp();	

			rootScene = InitUI.sceneInitUI;	

			//TEST CSS FRONTEND
			//Charger la feuille de style css dans la scene :
			//InitUI.sceneInitUI.getStylesheets().add(getClass().getResource("./stylesheet.css").toExternalForm());
			//InitUI.valid.setId("valid");

			primaryStage.setScene(rootScene);	// prend le retour de la m�thode initUI
			primaryStage.setTitle(frameTitle);
			primaryStage.sizeToScene();	
			primaryStage.setResizable(false);
			primaryStage.show();

			InitUI.valid.setOnAction((event) -> {	
				try {
					if (bdd.exists()) {
						if (InitUI.pwTf.getText().length() > 0) { 
							FileWriter fw = new FileWriter(passFile, false);	// possibilit� de hasher le code
							fw.write(InitUI.pwTf.getText());
							fw.close();
							importFile();
							rootScene = MainUI.mainPanel();
							primaryStage.setScene(rootScene);
							primaryStage.setTitle(frameTitle);
							primaryStage.sizeToScene();	
							primaryStage.setResizable(true);
							primaryStage.setMaximized(true);
							adminApp();
							majApp();
						} else {
							InitUI.pwTf.setPromptText("Mot de passe trop court");
							InitUI.noBdd.setText("");
						}
					} else InitUI.noBdd.setText("Pas de fichier");	// on ne sort jamais de la fen�tre de d�marrage tant que stagiaires.txt pas pr�sent

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			});
		} else {
			rootScene = MainUI.mainPanel();	// tous les fichiers n�cessaires au bon fonctionnement du programme sont pr�sents
			primaryStage.setScene(rootScene);
			primaryStage.setTitle(frameTitle);
			primaryStage.sizeToScene();	
			primaryStage.setResizable(true);
			primaryStage.show();
			primaryStage.setMaximized(true);
			adminApp();
			majApp();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	public static void importFile() {
		Import  i = new Import();
		try {
			i.importFileStagiaire();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void closeAdminApp() {	// cette m�thode rend la stage de la fen�tre admin accessible depuis la classe adminUI
		secondaryStage.close();
	}
	
	public static void showApp(Scene s) {    // cette m�thode rend la stage de la fen�tre admin accessible depuis la classe adminUI
        secondaryStage.setScene(s);
        secondaryStage.show();
    }

	public static void adminApp() {
		MainUI.admin.setOnAction((event) -> {
			AdminUI.adminPanel();
			if (AdminUI.user) {
				AdminUI.errorMessage.setText("");
				secondaryStage.setScene(AdminUI.sceneAdminUI);
				secondaryStage.setTitle("Authentification Administrateur");
				secondaryStage.sizeToScene();
				secondaryStage.setResizable(false);
				secondaryStage.show();

			}
			else {
				MainUI.btn3.setVisible(false);
				MainUI.btn4.setVisible(false);
				AdminUI.user=true;
				MainUI.admin.setText("Admin");
				Stage stage = (Stage) rootScene.getWindow();
				stage.setTitle("Utilisateur : Gestionnaire de stagiaires");
			}
		});
	}

	public static void majApp() {
		MainUI.btn4.setOnAction((event) -> {
			secondaryStage.setScene(MajData.Maj(MainUI.tf1.getText(),MainUI.tf2.getText(),MainUI.tf3.getText(),MainUI.tf4.getText(),MainUI.tf5.getText()));
			secondaryStage.setTitle("Modification et mise � jour d'un stagiaire");
			secondaryStage.sizeToScene();
			secondaryStage.setResizable(false);
			secondaryStage.show();
		});
	}
}
