package userInterface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class AdminUI {

	protected static Scene sceneAdminUI;
	protected static boolean user = true;
	protected static PasswordField txtPwEntered;

	protected static Label errorMessage = new Label("");
	protected static Label lblLogOn;

	public static Scene adminPanel() {

		//FRONT
		//secondaryRoot panel (pop-up de connexion suite � clic sur le bouton "admin" / contains vBox2)
		AnchorPane secondaryRoot = new AnchorPane();

		VBox vBox2 = new VBox(30);

		//secondaryRoot //gridPane (contained in vBox2)		
		GridPane gridPane = new GridPane();

		lblLogOn= new Label("Veuillez renseigner votre mot de passe :");
		txtPwEntered = new PasswordField();
		txtPwEntered.setPrefWidth(200);

		gridPane.addRow(0, lblLogOn);
		gridPane.addRow(1, txtPwEntered, errorMessage);
		gridPane.setVgap(20);
		gridPane.setHgap(20);
		gridPane.setAlignment(Pos.CENTER_LEFT);

		//secondaryRoot //logOnBox (contained in vBox2) (PART 1)	
		HBox logOnBox = new HBox(80);

		Button logOn = new Button("Se connecter");
		logOn.setPrefSize(200, 50);

		Button pwChange = new Button("Modifier mot de passe");
		pwChange.setPrefSize(200, 50);

		logOnBox.getChildren().addAll(logOn,pwChange);
		logOnBox.setAlignment(Pos.CENTER_LEFT);

		//secondaryRoot //logOnBox (contained in vBox2) (PART 2)	
		logOn.setOnAction(new EventHandler<ActionEvent>() {/// EDIT SIMON : m�thode associ�e � l'event d'affichage des boutons "suppression" et "mise-�-jour" en mode admin

			@Override
			public void handle(ActionEvent event) {
				FileReader fr;
				try {
					fr = new FileReader(new File(MainApp.relativepath + "/Bin/acs"));
					BufferedReader br = new BufferedReader(fr);

					if (txtPwEntered.getText().toString().equals(br.readLine())) {
						MainUI.btn3.setVisible(true); /// EDIT SIMON : affichage du bouton en mode admin
						//btn3.setManaged(false);
						MainUI.btn4.setVisible(true); /// EDIT SIMON : affichage du bouton en mode admin
						//btn4.setManaged(false);
						MainUI.admin.setText("User");
						Stage stage = (Stage) MainApp.rootScene.getWindow();
						stage.setTitle("Administrateur : Gestionnaire de stagiaires");
						txtPwEntered.clear();
						errorMessage.setText("");

						user=false;
						MainApp.closeAdminApp();
						br.close();
					} else {
						errorMessage.setText("Mot de passe incorrect");
						txtPwEntered.clear();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		pwChange.setOnAction((event) -> {
			try {
				FileReader fr = new FileReader(new File(MainApp.relativepath + "/Bin/acs"));
				BufferedReader br = new BufferedReader(fr);
				boolean passVerif = txtPwEntered.getText().toString().equals(br.readLine());

				errorMessage.setText("");

				if (pwChange.getText().equals("Modifier mot de passe")) {
					txtPwEntered.clear();
					lblLogOn.setText("Veuillez entrer votre mot de passe actuel");
					if (!passVerif) {
						if (txtPwEntered.getText().length() > 0) {errorMessage.setText("Mot de passe incorrect");}
						txtPwEntered.clear();
						txtPwEntered.setPromptText("Mot de passe actuel");
					} else {
						pwChange.setText("Valider");
						lblLogOn.setText("Veuillez entrer un nouveau mot de passe");
						txtPwEntered.setPromptText("Nouveau mot de passe");

					} 
				} else {
					if (txtPwEntered.getText().length() > 0) { 
						lblLogOn.setText("Votre mot de passe a bien �t� modifi�");
						FileWriter fw = new FileWriter(MainApp.passFile, false);
						fw.write(txtPwEntered.getText());
						fw.close();
						txtPwEntered.clear();
						pwChange.setText("Modifier mot de passe");
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		//gridPane et logOnBox constituent la vBOX2
		vBox2.getChildren().addAll(gridPane,logOnBox);
		vBox2.setAlignment(Pos.CENTER_LEFT);
		vBox2.setPadding(new Insets(40));

		//MANAGE secondaryRoot
		secondaryRoot.getChildren().add(vBox2);
		AnchorPane.setTopAnchor(vBox2, 1.);
		AnchorPane.setBottomAnchor(vBox2, 1.);
		AnchorPane.setLeftAnchor(vBox2, 1.);
		AnchorPane.setRightAnchor(vBox2, 1.);

		//FRONTEND TEST
		if (MainApp.theme.equals(MainApp.b)) {
			secondaryRoot.setStyle("-fx-background-image: url("+MainApp.theme+")");
			darkTheme();
		} else if (MainApp.theme.equals(MainApp.a)) {
			secondaryRoot.setStyle("-fx-background-image: url("+MainApp.theme+")");
		} else secondaryRoot.setStyle("-fx-background-color:skyblue");
		secondaryRoot.setPrefSize(600, 200);

		sceneAdminUI = new Scene(secondaryRoot);
		return sceneAdminUI;
	}
	public static void darkTheme() {
		DropShadow shadow = new DropShadow();
		shadow.setRadius(60);
		shadow.setSpread(.85);
		lblLogOn.setEffect(shadow);
		errorMessage.setEffect(shadow);
		lblLogOn.setTextFill(Color.WHITE);
		errorMessage.setTextFill(Color.WHITE);
	}
}
